const path = require('path');
const dist = path.resolve(__dirname, 'dist');

module.exports = {
	entry: './src/index.js',
	output: {
		filename: 'main.js',
		path: dist
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				use: {
					loader: 'babel-loader'
				}
			},
			{
				test: /\.css$/,
				use: [ 'style-loader', 'css-loader' ]
			},
			{
				test: /\.(ttf|eot|woff|woff2)$/,
				use: {
					loader: 'file-loader',
					options: {
						name: 'fonts/[name].[ext]',
					},
				},
			}
		]
	}
};

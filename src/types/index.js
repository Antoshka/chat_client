// @flow strict
export type LoadingStateType = 'idle' | 'loading' | 'success' | 'failure';

export type State = {
	checkToken: {
		loadingState: LoadingStateType,
		error: string
	},
	signUp: {
		loadingState: LoadingStateType,
		error: string
	},
	signIn: {
		loadingState: LoadingStateType,
		error: string
	},
	isAuth: boolean
};

export type CheckTokenRequestAction = { type: 'CHECK_TOKEN_REQUEST' };
export type CheckTokenSuccessAction = { type: 'CHECK_TOKEN_SUCCESS' };
export type CheckTokenFailureAction = { type: 'CHECK_TOKEN_FAILURE', payload: string };
export type SignUpRequestAction = { type: 'SIGN_UP_REQUEST', payload: { login: string, password: string } };
export type SignUpSuccessAction = { type: 'SIGN_UP_SUCCESS' };
export type SignUpFailureAction = { type: 'SIGN_UP_FAILURE', payload: string };
export type SignInRequestAction = { type: 'SIGN_IN_REQUEST', payload: { login: string, password: string } };
export type SignInSuccessAction = { type: 'SIGN_IN_SUCCESS' };
export type SignInFailureAction = { type: 'SIGN_IN_FAILURE', payload: string };
export type SignOutAction = { type: 'SIGN_OUT' };
export type Action = 
	| CheckTokenRequestAction 
	| CheckTokenSuccessAction 
	| CheckTokenFailureAction
	| SignUpRequestAction
	| SignUpSuccessAction
	| SignUpFailureAction
	| SignInRequestAction
	| SignInSuccessAction
	| SignInFailureAction
	| SignOutAction;
// @flow strict
import type {
	CheckTokenRequestAction,
	CheckTokenSuccessAction,
	CheckTokenFailureAction,
	SignUpRequestAction,
	SignUpSuccessAction,
	SignUpFailureAction,
	SignInRequestAction,
	SignInSuccessAction,
	SignInFailureAction,
	SignOutAction,
	Action
} from '../types';

const checkTokenRequest = (): CheckTokenRequestAction => ({
	type: 'CHECK_TOKEN_REQUEST'
});

const checkTokenSuccess = (): CheckTokenSuccessAction => ({
	type: 'CHECK_TOKEN_SUCCESS'
});

const checkTokenFailure = (error: string): CheckTokenFailureAction => ({ 
	type: 'CHECK_TOKEN_FAILURE',
	payload: error
});

const signUpRequest = (payload: { login: string, password: string }): SignUpRequestAction => ({
	type: 'SIGN_UP_REQUEST',
	payload: payload
});

const signUpSuccess = (): SignUpSuccessAction => ({ 
	type: 'SIGN_UP_SUCCESS' 
});

const signUpFailure = (error: string): SignUpFailureAction => ({
	type: 'SIGN_UP_FAILURE',
	payload: error
});

const signInRequest = (payload: { login: string, password: string }): SignInRequestAction => ({
	type: 'SIGN_IN_REQUEST',
	payload: payload
});

const signInSuccess = (): SignInSuccessAction => ({
	type: 'SIGN_IN_SUCCESS' 
});

const signInFailure = (error: string): SignInFailureAction => ({
	type: 'SIGN_IN_FAILURE',
	payload: error
});

const signOut = (): SignOutAction => ({
	type: 'SIGN_OUT'
});

export {
	checkTokenRequest,
	checkTokenSuccess,
	checkTokenFailure,
	signUpRequest,
	signUpSuccess,
	signUpFailure,
	signInRequest,
	signInSuccess,
	signInFailure,
	signOut
};

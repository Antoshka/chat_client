// @flow strict
import type {
	Action, 
	State, 
	LoadingStateType
} from '../types';
import {
	checkTokenRequest,
	checkTokenSuccess,
	checkTokenFailure,
	signUpRequest,
	signUpSuccess,
	signUpFailure,
	signInRequest,
	signInSuccess,
	signInFailure
} from '../actions';

const defaultState: State = {
	checkToken: {
		loadingState: 'idle',
		error: ''
	},
	signUp: {
		loadingState: 'idle',
		error: ''
	},
	signIn: {
		loadingState: 'idle',
		error: ''
	},
	isAuth: false
};

const reducer = (state: State = defaultState, action: Action): State => {
	switch(action.type) {
		case 'CHECK_TOKEN_REQUEST':
			return {
				...state,
				checkToken: {
					loadingState: 'loading',
					error: ''
				}
			};
		case 'CHECK_TOKEN_SUCCESS':
			return {
				...state,
				checkToken: {
					loadingState: 'success',
					error: ''
				},
				isAuth: true
			};
		case 'CHECK_TOKEN_FAILURE':
			return {
				...state,
				checkToken: {
					loadingState: 'failure',
					error: action.payload
				}
			};
		case 'SIGN_UP_REQUEST':
			return {
				...state,
				signUp: {
					loadingState: 'loading',
					error: ''
				}
			};
		case 'SIGN_UP_SUCCESS':
			return {
				...state,
				signUp: {
					loadingState: 'success',
					error: ''
				},
				isAuth: true
			};
		case 'SIGN_UP_FAILURE':
			return {
				...state,
				signUp: {
					loadingState: 'failure',
					error: action.payload
				}
			};
		case 'SIGN_IN_REQUEST':
			return {
				...state,
				signIn: {
					loadingState: 'loading',
					error: ''
				}
			};
		case 'SIGN_IN_SUCCESS':
			return {
				...state,
				signIn: {
					loadingState: 'success',
					error: ''
				},
				isAuth: true
			};
		case 'SIGN_IN_FAILURE':
			return {
				...state,
				signIn: {
					loadingState: 'failure',
					error: action.payload
				}
			};
		case 'SIGN_OUT':
			return {
				...state,
				isAuth: false
			};
		default:
			(action: empty);
			return state;
	}
};

export default reducer;

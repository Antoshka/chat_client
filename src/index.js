// @flow strict
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import { createAppStore } from './store';
import App from './containers/App/App';
import './index.css';

const store = createAppStore();
const root: ?Element = document.getElementById('root');

if (root) {
    ReactDOM.render(
        <Provider store={store}>
            <BrowserRouter>
                <App />
            </BrowserRouter>
        </Provider>,
        root
    );
}


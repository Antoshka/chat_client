// @flow strict
import * as React from 'react';
import {Link} from 'react-router-dom';
import classNames from 'classnames';
import './SignUp.css';

type Props = {
	login: string,
	password: string,
	isLoading: boolean,
	isSuccess: boolean,
	isError: boolean,
	error: ?string,
	onChangeLogin: (e: SyntheticEvent<HTMLInputElement>) => void,
	onChangePassword: (e: SyntheticEvent<HTMLInputElement>) => void,
	onSubmit: (e: SyntheticEvent<HTMLFormElement>) => void
};

const SignUp = (props: Props): React.Element<'div'> => {
	const renderButton = (): React.Element<'button'> => {
		let text = 'Зарегистрироваться';
		
		if (props.isLoading) {
			text = 'Регистрируем...';
		} else if (props.isSuccess) {
			text = 'Ты зарегистрирован!';
		}
		
		return (
			<button className={classNames('SignUp_form-submit', {'SignUp_form-submit--success': props.isSuccess})}>
				{text}
			</button>
		);
	};
	
	return (
		<div className="SignUp">
			<form className="SignUp_form"
			      onSubmit={props.onSubmit}
			>
				{props.isError &&
				<div className="SignUp_error">
					{props.error}
				</div>
				}
				
				<label htmlFor="SignUp_form-login" className="SignUp_form-field">
					<span className="SignUp_form-fieldname">Логин:</span>
					<input id="SignUp_form-login"
					       type="text"
						   className="SignUp_form-login"
						   value={props.login}
					       onChange={props.onChangeLogin}
					/>
				</label>
				<label htmlFor="SignUp_form-password" className="SignUp_form-field">
					<span className="SignUp_form-fieldname">Пароль:</span>
					<input id="SignUp_form-password"
					       type="password"
					       className="SignUp_form-password"
					       value={props.password}
					       onChange={props.onChangePassword}
					/>
				</label>
				{renderButton()}
				<Link to="/" className="SignUp_link">У меня уже есть аккаунт</Link>
			</form>
		</div>
	);
};

export default SignUp;

// @flow strict
import * as React from 'react';
import SignOut from '../SignOut/SignOut';
import './Chat.css';

type Props = {
	signOutHandler: () => void
};

const Chat = (props: Props): React.Element<'div'> => {
	return (
		<div className="Chat">
			<SignOut onClick={props.signOutHandler}/>
		</div>
	);
};

export default Chat;

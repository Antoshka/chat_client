// @flow strict
import * as React from 'react';
import './NotFound.css';

const NotFound = (): React.Element<'div'> => {
	return (
		<div className="NotFound">
			404
		</div>
	);
};

export default NotFound;

// @flow strict
import * as React from 'react';
import './SignOut.css';

type Props = {
    onClick: (e: SyntheticEvent<HTMLButtonElement>) => void
};

const SignOut = (props: Props): React.Element<'button'> => {
    return (
        <button className="SignOut" onClick={props.onClick}>Выйти</button>
    );
};

export default SignOut;
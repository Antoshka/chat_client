// @flow strict
import * as React from 'react';
import {Link} from 'react-router-dom';
import './SignIn.css';

type Props = {
	login: string,
	password: string,
	isLoading: boolean,
	isError: boolean,
	error: ?string,
	onChangeLogin: (e: SyntheticEvent<HTMLInputElement>) => void,
	onChangePassword: (e: SyntheticEvent<HTMLInputElement>) => void,
	onSubmit: (e: SyntheticEvent<HTMLFormElement>) => void
};

const SignIn = (props: Props): React.Element<'div'> => {
	return (
		<div className="SignIn">
			<form className="SignIn_form"
			      onSubmit={props.onSubmit}
			>
				{props.isError &&
				<div className="SignIn_error">
					{props.error}
				</div>
				}
				
				<label htmlFor="SignIn_form-login" className="SignIn_form-field">
					<span className="SignIn_form-fieldname">Логин:</span>
					<input id="SignIn_form-login"
					       type="text"
					       className="SignIn_form-login"
					       value={props.login}
					       onChange={props.onChangeLogin}
					/>
				</label>
				<label htmlFor="SignIn_form-password" className="SignIn_form-field">
					<span className="SignIn_form-fieldname">Пароль:</span>
					<input id="SignIn_form-password"
					       type="password"
					       className="SignIn_form-password"
					       value={props.password}
					       onChange={props.onChangePassword}
					/>
				</label>
				<button className="SignIn_form-submit">
					{props.isLoading ? 'Ожидайте...' : 'Войти'}
				</button>
				<Link to="/signup" className="SignIn_link">У меня еще нет аккаунта</Link>
			</form>
		</div>
	);
};

export default SignIn;

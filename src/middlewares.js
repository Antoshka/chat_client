// @flow strict
import type {Middleware} from 'redux';
import type {State, Action} from './types';
import {
	checkTokenRequest,
	checkTokenSuccess,
	checkTokenFailure,
	signUpRequest,
	signUpSuccess,
	signUpFailure,
	signInRequest,
	signInSuccess,
	signInFailure
} from './actions';

async function request(url: string, data: {} = {}) {
	const response = await fetch(`/a/${url}`, {
		method: 'POST',
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/json'
		},
		body: JSON.stringify(data)
	});
	
	if (response.status !== 200) {
		throw new Error(getErrorText());
	}
	
	const responseData = await response.json();
	
	if (responseData && typeof responseData.status === 'number') {
		if (responseData.status === 0) {
			return responseData;
		} else {
			throw new Error(responseData.error);
		}
	} else {
		throw new Error(getErrorText());
	}
}

const getErrorText = (error: ?string): string => {
	return error || 'Что-то пошло не так.';
};

export const checkToken: Middleware<State, Action> = store => next => action => {
	if (action.type === 'CHECK_TOKEN_REQUEST') {
		const token = getCookie('token');
		
		if (token) {
			request('sign/check').then(response => {
				store.dispatch(checkTokenSuccess());
			}).catch(e => {
				store.dispatch(checkTokenFailure(e.message));
			});
		} else {
			setTimeout(() => {
				store.dispatch(checkTokenFailure('Токен отсутствует'));
			}, 0);
		}
	}
	
	return next(action);
};

export const signUp: Middleware<State, Action> = store => next => action => {
	if (action.type === 'SIGN_UP_REQUEST') {
		request('sign/up', action.payload).then(response => {
			setCookie('token', response.data.token);
			store.dispatch(signUpSuccess());
		}).catch(e => {
			store.dispatch(signUpFailure(e.message));
		});
	}
	
	return next(action);
};

export const signIn: Middleware<State, Action> = store => next => action => {
	if (action.type === 'SIGN_IN_REQUEST') {
		request('sign/in', action.payload).then((response) => {
			setCookie('token', response.data.token);
			store.dispatch(signInSuccess());
		}).catch((e) => {
			store.dispatch(signInFailure(getErrorText()));
		});
	}
	
	return next(action);
};

export const signOut: Middleware<State, Action> = store => next => action => {
	if (action.type === 'SIGN_OUT') {
		deleteCookie('token');
	}
	
	return next(action);
};

const setCookie = (name: string, value: string): void => {
	const date: Date = new Date();
	
	date.setTime(date.getTime() + (365 * 24 * 60 * 60 * 1000));
	
	const expires = '; expires=' + date.toUTCString();
	
	document.cookie = name + '=' + value + expires + '; Path=/;';
};

const getCookie = (name: string): ?string => {
	const matches = document.cookie.match(new RegExp(
		"(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
	));
	
	return matches ? decodeURIComponent(matches[1]) : undefined;
};

const deleteCookie = (name: string): void => {
    document.cookie = name + '=; Expires=Thu, 01 Jan 1970 00:00:01 GMT;path=/';
};

// @flow strict
import {createStore, compose, applyMiddleware} from 'redux';
import reducers from './reducers';
import {
	checkToken,
	signUp,
	signIn,
	signOut
} from './middlewares';

export const createAppStore = () => {
	return createStore(
		reducers,
		compose(
			applyMiddleware(checkToken),
			applyMiddleware(signUp),
			applyMiddleware(signIn),
			applyMiddleware(signOut),
			reduxDevToolsExtension()
		)
	);
};

const reduxDevToolsExtension = () => {
	if (window.__REDUX_DEVTOOLS_EXTENSION__) {
		return window.__REDUX_DEVTOOLS_EXTENSION__();
	}
	
	return applyMiddleware(store => next => action => next(action));
};

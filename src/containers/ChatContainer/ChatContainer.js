// @flow strict
import type {State} from '../../types';
import * as React from 'react';
import {connect} from 'react-redux';
import Chat from '../../components/Chat/Chat';
import {signOut} from '../../actions';

type Props = {
	signOut: (typeof signOut)
};

class ChatContainer extends React.Component<Props> {
	constructor(props) {
		super(props);
	}

	signOutHandler = () => {
		this.props.signOut();
	};
	
	render(): React.Element<typeof Chat> {
		return <Chat signOutHandler={this.signOutHandler}/>;
	}
}

const mapStateToProps = (state: State) => {
	return {};
};

const mapDispatchToProps = {signOut};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(ChatContainer);

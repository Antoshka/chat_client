// @flow strict
import type {State as ReduxState} from '../../types';
import * as React from 'react';
import {connect} from 'react-redux';
import SignUp from '../../components/SignUp/SignUp';
import {signUpRequest, signUpFailure} from '../../actions';

type Props = {
	isLoading: boolean,
	isSuccess: boolean,
	isError: boolean,
	error: string,
	signUpRequest: (typeof signUpRequest),
	signUpFailure: (typeof signUpFailure)
};

type State = {
	login: string,
	password: string
};

class SignUpContainer extends React.Component<Props, State> {
	constructor(props) {
		super(props);
		
		this.state = {
			login: '',
			password: ''
		};
	}
	
	checkLogin = (login: string): boolean => {
		return !!login.length;
	};
	
	checkPassword = (password: string): boolean => {
		return !!password.length;
	};
	
	handleLoginChange = (e: SyntheticEvent<HTMLInputElement>): void => {
		const {isLoading, isSuccess} = this.props;
		
		if (!isLoading && !isSuccess) {
			this.setState({ login: e.currentTarget.value });
		}
	};
	
	handlePasswordChange = (e: SyntheticEvent<HTMLInputElement>): void => {
		const {isLoading, isSuccess} = this.props;
		
		if (!isLoading && !isSuccess) {
			this.setState({ password: e.currentTarget.value });
		}
	};
	
	handleSubmit = (e: SyntheticEvent<HTMLFormElement>): void => {
		const {login, password} = this.state;
		const {isLoading, isSuccess} = this.props;
		
		e.preventDefault();
		
		if (!isLoading && !isSuccess) {
			if (this.checkLogin(login) && this.checkPassword(password)) {
				this.props.signUpRequest({login, password});
			} else {
				this.props.signUpFailure('Укажи логин и пароль!');
			}
		}
	};
	
	render(): React.Element<typeof SignUp> {
		const { login, password } = this.state;
		const { isLoading, isSuccess, isError, error } = this.props;
		
		return <SignUp login={login}
		               password={password}
		               isLoading={isLoading}
		               isSuccess={isSuccess}
		               isError={isError}
		               error={error}
		               onChangeLogin={this.handleLoginChange}
		               onChangePassword={this.handlePasswordChange}
		               onSubmit={this.handleSubmit}
		/>;
	}
}

const mapStateToProps = (state: ReduxState) => {
	return {
		isLoading: (state.signUp.loadingState === 'loading'),
		isSuccess: (state.signUp.loadingState === 'success'),
		isError: (state.signUp.loadingState === 'failure'),
		error: state.signUp.error
	};
};

const mapDispatchToProps = {signUpRequest, signUpFailure};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(SignUpContainer);

// @flow strict
import type {LoadingStateType} from '../../types';
import type {State} from '../../types';
import React from 'react';
import {Route, Switch, Redirect, Link, withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import SignUpContainer from '../SignUpContainer/SignUpContainer';
import SignInContainer from '../SignInContainer/SignInContainer';
import ChatContainer from '../ChatContainer/ChatContainer';
import NotFound from '../../components/NotFound/NotFound';
import {checkTokenRequest} from '../../actions';

type Props = {
	checkTokenLoadingState: LoadingStateType,
	isAuth: boolean,
	checkTokenRequest: (typeof checkTokenRequest)
};

class App extends React.Component<Props> {
	render() {
		const {isAuth, checkTokenLoadingState, checkTokenRequest} = this.props;
		
		if (checkTokenLoadingState === 'idle') {
			checkTokenRequest();
		}
		
		if (checkTokenLoadingState === 'success' || checkTokenLoadingState === 'failure') {
			return (
				<div className="App">
					<Switch>
						<Route exact path="/" render={() => isAuth ? <Redirect to="/chat"/> : <SignInContainer/>}/>
						<Route exact path="/signup" render={() => isAuth ? <Redirect to="/chat"/> : <SignUpContainer/>}/>
						<Route exact path="/chat" render={() => isAuth ? <ChatContainer/> : <Redirect to="/"/>}/>
						<Route component={NotFound} />
					</Switch>
				</div>
			);
		} else {
			return 'Loading...';
		}
	}
}

const mapStateToProps = (state: State) => {
	return {
		checkTokenLoadingState: state.checkToken.loadingState,
		isAuth: state.isAuth
	};
};

const mapDispatchToProps = {checkTokenRequest};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));

// @flow strict
import type {State as ReduxState} from '../../types';
import * as React from 'react';
import {connect} from 'react-redux';
import SignIn from '../../components/SignIn/SignIn';
import {signInRequest, signInFailure} from '../../actions';

type Props = {
	isLoading: boolean,
	isSuccess: boolean,
	isError: boolean,
	error: ?string,
	signInRequest: (typeof signInRequest),
	signInFailure: (typeof signInFailure)
};

type State = {
	login: string,
	password: string
};

class SignInContainer extends React.Component<Props, State> {
	constructor(props) {
		super(props);
		
		this.state = {
			login: '',
			password: ''
		};
	}
	
	checkLogin = (login: string): boolean => {
		return !!login.length;
	};
	
	checkPassword = (password: string): boolean => {
		return !!password.length;
	};
	
	handleLoginChange = (e: SyntheticEvent<HTMLInputElement>): void => {
		const {isLoading, isSuccess} = this.props;
		
		if (!isLoading && !isSuccess) {
			this.setState({ login: e.currentTarget.value });
		}
	};
	
	handlePasswordChange = (e: SyntheticEvent<HTMLInputElement>): void => {
		const {isLoading, isSuccess} = this.props;
		
		if (!isLoading && !isSuccess) {
			this.setState({ password: e.currentTarget.value });
		}
	};
	
	handleSubmit = (e: SyntheticEvent<HTMLFormElement>): void => {
		const {login, password} = this.state;
		const {isLoading, isSuccess} = this.props;
		
		e.preventDefault();
		
		if (!isLoading && !isSuccess) {
			if (this.checkLogin(login) && this.checkPassword(password)) {
				this.props.signInRequest({login, password});
			} else {
				this.props.signInFailure('Укажи логин и пароль!');
			}
		}
	};
	
	render(): React.Element<typeof SignIn> {
		const { login, password } = this.state;
		const {isLoading, isSuccess, isError, error} = this.props;
		
		return <SignIn login={login}
		               password={password}
		               isLoading={isLoading}
		               isError={isError}
		               error={error}
		               onChangeLogin={this.handleLoginChange}
		               onChangePassword={this.handlePasswordChange}
		               onSubmit={this.handleSubmit}
		/>;
	}
}

const mapStateToProps = (state: ReduxState) => {
	return {
		isLoading: (state.signIn.loadingState === 'loading'),
		isSuccess: (state.signIn.loadingState === 'success'),
		isError: (state.signIn.loadingState === 'failure'),
		error: state.signIn.error
	};
};

const mapDispatchToProps = {signInRequest, signInFailure};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(SignInContainer);
